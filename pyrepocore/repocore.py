# coding: utf-8

# pyrepocore
# Copyright © 736 Computing Services Limited 2012
# Apache 2.0 licenced - http://opensource.736cs.com/licenses/apache2

from __future__ import print_function

import requests
import json


class APIError(StandardError):
    pass


class Connection:
    """
    Connection: main class of pyrepocore

    Provides an instance of the Repocore API

    Example:

    import pyrepocore

    r = pyrepocore.Connection(
        username="u53rn4m3",
        password="p455w0rd",
        server="mysite.repocore.net",
        ssl=True
    )
    """

    def __init__(self, username, password, server, ssl=True):
        
        # Put connection info into wider namespace
        self.username = username
        self.password = password
        self.server = server
        self.ssl = ssl

        self.__req_settings = dict(
            auth=(self.username, self.password),
            verify=False
        )

        # Check if the API is up
        self.__ping()

    @property
    def server_url(self):
        """
        Output a fully qualified server URL
        based on SSL settings.
        """
        uri = "http://" + self.server

        if self.ssl:
            uri = "https://" + self.server

        return uri

    def __url(self, *args):
        """
        Construct an API URL from server_url
        and any arguments passed to the method

        All arguments should be strings.
        """ 
        uri = self.server_url + "/api"

        for arg in args:
            if arg is not None:
                uri += "/" + arg

        return uri

    def __get(self, url):
        """
        Runs requests.get() with the credentials
        provided when instantiating the connection
        object.

        Keyword arguments:

        url -- the request URL
        """
        return requests.get(url, **self.__req_settings)

    def __put(self, url, data):
        """
        Runs requests.put() with the credentials
        provided when instantiating the connection
        object.

        Keyword arguments:

        url -- the request URL
        data -- request data to send to the URL
        """
        return requests.put(url, data=data, **self.__req_settings)

    def __post(self, url, **kwargs):
        """
        Runs requests.post() with the credentials
        provided when instantiating the connection
        object.

        Keyword arguments:

        url -- the request URL
        **kwargs -- keyword arguments passed
                    directly to requests.post()
        """
        return requests.post(url, **dict(kwargs, **self.__req_settings))

    def __delete(self, url, data):
        """
        Runs requests.delete() with the credentials
        provided when instantiating the connection
        object.

        Keyword arguments:

        url -- the request URL
        data -- request data to send to the URL
        """
        return requests.delete(url, data=data, **self.__req_settings)

    def __ping(self):
        """
        Hits the ping API endpoint to check if the
        server is up. Raises an exception if it fails.
        """
        try:
            request = self.__get(self.__url("ping"))
        except requests.exceptions.ConnectionError as err:
            raise APIError(
                "Cannot connect to Repocore server: {0}".format(err)
            )

        if request.status_code == 204:
            return True
        else:
            raise APIError(
                "Cannot connect to Repocore server: " +
                "The API seems to be down."
            )

    """
    Package management API
    """

    def get_packages(self, package=None):
        """
        Returns a dict() with a package or list of
        packages.

        Keyword arguments:

        package -- the package to be looked up. If 
        None, the full list of packages is returned.
        (default None)
        """

        request = self.__get(self.__url("packages", package))

        if request.status_code == 404 and package is not None:
            raise APIError("Package not found")

        try:
            return json.loads(request.text)
        except ValueError:
            return (request.status_code, request.text)

    def get_package(self, package):
        """
        Alias of get_packages except with package
        as required argument
        """

        return self.get_packages(package)

    def set_package_from_url(self, upload_url, dist, mirror_url=None):
        """
        Passes a package URL to the Repocore API.

        Keyword arguments:

        upload_url -- the URL to download the package from
        dist -- the distribution the package will be saved to
        mirror_url (optional) -- a permanent link to the package file
        """

        payload = {'upload_url': upload_url, 'dist': dist}

        if mirror_url:
            payload['mirror_url'] = mirror_url

        request = self.__post(self.__url("packages"), data=payload)

        if request.status_code == 415:
            raise APIError("Invalid or unrecognised package format.")

        try:
            return json.loads(request.text)
        except ValueError:
            return (request.status_code, request.text)

    def set_package_from_filename(self, filename, dist):
        """
        Uploads a package to Repocore directly, with
        the filename as the method input

        Keyword arguments:

        filename -- the filename for the package to
                    be uploaded
        dist -- the distribution the package will be
                saved to
        """
        files = {'package': open(filename, 'rb')}
        payload = {'dist': dist}

        request = self.__post(
            self.__url("packages"),
            files=files,
            data=payload
        )

        if request.status_code == 415:
            raise APIError("Invalid or unrecognised package format.")

        try:
            return json.loads(request.text)
        except ValueError:
            return (request.status_code, request.text)

    def set_package_from_file(self, handle, dist):
        """
        Uploads a package to Repocore directly, with
        a file handle as the method input

        Keyword arguments:
        handle -- a Python file object containing a
                  package
        dist -- the distribution the package will be
                saved to
        """

        files = {'package': handle}
        payload = {'dist': dist}

        request = self.__post(
            self.__url("packages"),
            files=files,
            data=payload
        )

        if request.status_code == 415:
            raise APIError("Invalid or unrecognised package format.")

        try:
            return json.loads(request.text)
        except ValueError:
            return (request.status_code, request.text)

    def get_file_info(self, package, file_id):
        """
        Retrieves metadata information for an existing package file.

        Keyword arguments:

        package -- the name of the package 
        file_id -- the file ID for a specific package file
        """

        request = self.__get(self.__url("packages", package, "files", file_id))
        dists = self.__get(
            self.__url("packages", package, "files", file_id, "dists")
        )

        if request.status_code == 404:
            raise APIError("Package or file ID not found")

        info = json.loads(request.text)
        dist_info = json.loads(dists.text)

        return dict(info, **dist_info)

    def add_dists(self, package, file_id, *args):
        """
        Add one or more distributions to a package file

        Keyword arguments:

        package -- the name of the package
        file_id -- the file ID for a specific package

        Remaining arguments should consist of the
        distributions you want set on the package.
        """

        request = self.__put(
            self.__url("packages", package, "files", file_id),
            data=json.dumps({'add': args})
        )

        if request.status_code == 200:
            return True
        else:
            return False

    def set_dists(self, package, file_id, *args):
        """
        Same as add_dists(), but overwrites existing
        assigned distributions.
        """

        request = self.__put(
            self.__url("packages", package, "files", file_id),
            data=json.dumps({'set': args})
        )

        if request.status_code == 200:
            return True
        else:
            return False

    """
    User management API
    """

    def get_users(self, username=None):
        """
        Obtains the list of users.

        Keyword arguments:
        
        username (optional) -- username to look up.
        If None, returns the full list of users.
        """

        request = self.__get(self.__url("users", username))

        if request.status_code == 404 and username is not None:
            raise APIError("User not found")

        try:
            return json.loads(request.text)
        except ValueError:
            return (request.status_code, request.text)

    def get_user(self, username):
        """
        Same as get_users(), but username is not
        an optional argument.
        """
        return self.get_users(username)

    def set_user(self, username, **kwargs):
        """
        Create or update a user.

        Keyword arguments:
        
        username -- the username to be created or
        changed
        admin -- whether the user is an admin or
        not
        passblob -- super seekrit password used
        for API access
        repopass -- not-so-secret passphrase used
        to access repositories
        """

        payload = dict(**kwargs)
        request = self.__put(
            self.__url("users", username),
            data=json.dumps(payload)
        )

        if request.status_code != 200:
            raise APIError(
                "{0} error when creating user".format(request.status_code)
            )

        return True

    def get_user_packages(self, username):
        """
        Returns a dict() containing a list
        of all the packages the user has
        available.

        Keyword arguments:

        username -- the user to get the package
        list of
        """

        request = self.__get(self.__url("users", username, "packages"))

        if request.status_code == 404:
            raise APIError("User not found")

        try:
            return json.loads(request.text)
        except ValueError:
            return (request.status_code, request.text)

    def set_user_packages(self, username, packages):
        """
        Adds one or more packages to the user's
        list of available packages
        
        Keyword arguments:

        username -- the user to add packages to
        packages -- a list() containing the packages
        to add to the user
        """

        payload = {'packages': packages}
        request = self.__put(
            self.__url("users", username, "packages", packages),
            data=payload
        )

        return request.text

    def delete_user_package(self, username, package):
        """
        Deletes a package from the user's list of 
        available packages.

        Keyword arguments:

        username -- the user to remove packages from
        package -- the package to remove
        """

        request = self.__delete(
            self.__url("users", username, "packages", package)
        )

        return True

    def get_user_repository_files(self, username, repo_type, distro):
        """
        Returns a dict() containing URLs to repository
        files (e.g. repo install packages)

        Keyword arguments:

        username -- the username to retrieve package files for
        repotype -- apt or yum
        distro -- the distribution to retrieve package files for
        """

        if repo_type == "apt":
            files = dict(
                package=self.__url(
                    "repos", "apt", "files", distro,
                    username + "-main-repository.deb"
                ),
                aptconf=self.__url(
                    "repos", "apt", "files", "apt.conf"
                ),
                sourceslist=self.__url(
                    "repos", "apt", "files", distro,
                    username + "-main.list"
                ),
            )
        elif repo_type == "yum":
            files = dict(
                package=self.__url(
                    "repos", "yum", "files", distro,
                    username + "-main-repository.rpm"
                ),
                repofile=self.__url(
                    "repos", "yum", "files", distro,
                    username + "-main.repo"
                ),
            )
        else:
            raise APIError("Unsupported repository format")

        return files

    """
    Repository information
    """

    def get_repo_formats(self):
        """
        Returns the repository formats supported
        by Repocore as a dict().
        """

        request = self.__get(self.__url("repos"))

        try:
            return json.loads(request.text)
        except ValueError:
            return (request.status_code, request.text)

    def get_public_key(self):
        """
        Retrieves the PGP public key used to sign
        repositories.
        """

        request = self.__get(self.server_url + "/pubkey.gpg")
        return request.text

    def get_public_keyring(self):
        """
        Retrieves the binary public keyring used to
        sign repositories.
        """
        request = self.__get(self.server_url + "/pubring.gpg")
        return request.text
