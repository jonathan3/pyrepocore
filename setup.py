from setuptools import setup, find_packages
import sys, os

version = '0.1.1'

setup(name='pyrepocore',
      version=version,
      description="Python library for the Repocore service",
      long_description="""\
""",
      classifiers=[], # Get strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
      keywords='',
      author='Jonathan Prior',
      author_email='jjprior@736cs.com',
      url='http://opensource.736cs.com/',
      license='Apache License 2.0',
      packages=find_packages(exclude=['ez_setup', 'examples', 'tests']),
      include_package_data=True,
      zip_safe=False,
      install_requires=[
          'requests'
      ],
      entry_points="""
      # -*- Entry points: -*-
      """,
      )
